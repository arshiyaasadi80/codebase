import type { NextApiRequest, NextApiResponse } from 'next'

type Data = {
  ping: string
  req: NextApiRequest
}

export default function handler(req: NextApiRequest, res: NextApiResponse<Data>): void {
  res.status(200).json({ ping: 'pong', req })
}
