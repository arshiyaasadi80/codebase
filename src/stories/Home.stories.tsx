import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import Home from './../app/page'

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'pages/Home',
  component: Home,
} as ComponentMeta<typeof Home>

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
// const Template: ComponentStory<typeof Home> = ({ background }) => <Home background={background} />
const Template: ComponentStory<typeof Home> = () => <Home />
export const Index = Template.bind({})
Index.args = {}

// export const Info = Template.bind({})
// Info.args = {
//   background: 'info',
// }

// export const Success = Template.bind({})
// Success.args = {
//   background: 'success',
// }

// export const Warning = Template.bind({})
// Warning.args = {
//   background: 'warning',
// }

// export const Error = Template.bind({})
// Error.args = {
//   background: 'error',
// }
